package depth_first_search;
import java.util.*;

public class FlattenDictionary {
	static HashMap<String, String> flattenDictionary(HashMap<String, Object> dict) {
		// your code goes here

		HashMap<String, String> result = new HashMap<String, String>();
		DFS(dict, "", result);
		return result;
	}

	static void DFS(HashMap<String, Object> dict, String path, HashMap<String, String> result) {

		for (String key : dict.keySet()) {
			if (dict.get(key) instanceof String) {
				if (path != "" && key != "") {
					result.put(path + "." + key, (String) dict.get(key));
				} else if (path == "" || key == "") {
					result.put(path + key, (String) dict.get(key));
				} else {
					// result.put(path,(String)dict.get(key));
				}

			} else if (dict.get(key) instanceof Object) {
				if (path == "" || key == "") {
					DFS((HashMap<String, Object>) dict.get(key), path + key, result);
				} else if (path != "" && key != "") {
					DFS((HashMap<String, Object>) dict.get(key), path + "." + key, result);
				}
			}
		}

	}

	public static void main(String[] args) {

		HashMap<String, Object> dict = new HashMap<String, Object>();

		
		HashMap<String, String> keyco= new HashMap<String, String>();
		keyco.put("f", "4");
		
		HashMap<String, Object> keyc = new HashMap<String, Object>();
		keyc.put("d", "3");
		keyc.put("e", keyco);

		HashMap<String, Object> object = new HashMap<String, Object>();
		object.put("a", "2");
		object.put("b", "3");
		object.put("c", keyc);

		dict.put("Key1", "1");
		dict.put("Key2", object);
		
		//{"Key1":"1","Key2":{"a":"2","b":"3","c":{"d":"3","e":{"f":"4"}}}}

		System.out.println(flattenDictionary(dict));

	}
}
