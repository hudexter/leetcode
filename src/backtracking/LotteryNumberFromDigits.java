package backtracking;

import java.util.ArrayList;
import java.util.HashSet;

public class LotteryNumberFromDigits {

	public static void main(String[] args) {
		String[] inputs = { "569815571556", "4938532894754", "1234567", "472844278465445", "4938532842254", "", "1121111111111111111" };
		for (int i = 0; i < inputs.length; i++) {
			ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
			findLotteryPick(inputs[i], 0, new ArrayList<Integer>(), result);

			/*
			 * There could be more than 1 solution for one input, but we only print
			 * the first solution, e.g., 4938532842254 can form 49 38 53 28 4 22 54 or 49 38 53 28 42 2 54 or 49 38 53 28 42 25 4
			 */
			if (result.size() > 0) {
				System.out.println(inputs[i] + " -> " + list2string(result.get(0)));
				// this solution can also print all possible picks for one input string
//				for (int j = 0; j < result.size(); j++) {
//					System.out.println(inputs[i] + " -> " + list2string(result.get(j)));
//				}
			}
		}
	}

	/*
	 * Convert an integer list array to string, separated by space to format printing
	 * results
	 */
	public static String list2string(ArrayList<Integer> pick) {
		String result = "";
		for (int i = 0; i < pick.size(); i++) {
			if (i == 0) {
				result += pick.get(i);
			} else {
				result += " " + pick.get(i);
			}
		}
		return result;
	}

	/*
	 * Check if current string of digits represents a valid lottery number
	 */

	public static boolean isValidNumber(String digits) {
		try {
			int value = Integer.valueOf(digits);
			return value >= 1 && value <= 59;
		} catch (Exception e) {
			return false;
		}
	}

	/*
	 * Do backtracking search via recursion, starting from current digit at
	 * digits[start].
	 */

	public static void findLotteryPick(String digits, int start, ArrayList<Integer> temp,
			ArrayList<ArrayList<Integer>> result) {

		// Prune solution search space with obvious invalid condition that length is not valid
		if (digits.length() > 14 || digits.length() < 7) {
			return;
		}

		// One possible pick is found
		if (start == digits.length() && temp.size() == 7) {
			HashSet<Integer> set = new HashSet<Integer>();
			set.addAll(new ArrayList<Integer>(temp));
			if (set.size() == 7) { // make sure all elements of current pick are unique
				result.add(new ArrayList<Integer>(temp));
			}
			return;
		}
		

		// The digit string is too long, with already 7 numbers formed.
		if (start < digits.length() && temp.size() >= 7) {
			return;
		}

		// When current pointer is off limit
		if (start >= digits.length()) {
			return;
		}

		// Try to use 1 digit to form a lucky number
		String current = digits.substring(start, start + 1);
		if (isValidNumber(current)) {
			temp.add(Integer.valueOf(current));
			findLotteryPick(digits, start + 1, temp, result);
			temp.remove(temp.size() - 1);
		}

		// try to use 2 digits to form a lucky number
		if (start + 1 < digits.length()) {
			current = digits.substring(start, start + 2);
			if (isValidNumber(current)) {
				temp.add(Integer.valueOf(current));
				findLotteryPick(digits, start + 2, temp, result);
				temp.remove(temp.size() - 1);
			}
		}
	}

}
