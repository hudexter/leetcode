package backtracking;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gouthamvidyapradhan on 15/03/2017. Given a collection of distinct
 * numbers, return all possible permutations.
 * 
 * For example, [1,2,3] have the following permutations: [ [1,2,3], [1,3,2],
 * [2,1,3], [2,3,1], [3,1,2], [3,2,1] ]
 */
public class Permutations {
	/**
	 * Main method
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		int[] nums = { 1, 2, 3 };
		List<List<Integer>> result = new Permutations().permute(nums);
//		for(List<Integer> a: result) {
//			System.out.println(a.toString());
//		}
		
		result.forEach(n -> System.out.println(n));
	}

	public List<List<Integer>> permute(int[] nums) {
		List<List<Integer>> result = new ArrayList<>();
		nextPermutation(0, nums, result);
		return result;
	}

	private void nextPermutation(int i, int[] nums, List<List<Integer>> result) {
		if (i == nums.length - 1) {
			List<Integer> list = new ArrayList<>();
			for (int n : nums)
				list.add(n);
			result.add(list);
		} else {
			for (int j = i, l = nums.length; j < l; j++) {
				swap(i, j, nums);
				nextPermutation(i + 1, nums, result);
				swap(i, j, nums);
			}
		}
	}
	
	public void swap(int i, int j, int[] nums) {
		int temp = nums[i];
		nums[i] = nums[j];
		nums[j] = temp;
	}
}
