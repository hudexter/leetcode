package backtracking;

import java.util.ArrayList;
import java.util.HashSet;

public class CombinationString {
	
	public static void Combination(String s, int m) {
		
		ArrayList<String> result = new ArrayList<>();
		
		helper("ABCDEFGIJIKLMN", "", 2, result);
		
		for(String a: result) {
			System.out.println(a);
		}
		
	}
	
	public static void helper(String s, String temp,  int m, ArrayList<String> result) {
		
		if(m==0) {
			result.add(temp);
			return;
		}
		
		for(int i = 0; i < s.length(); i++) {
			helper(s.substring(i+1), temp+s.substring(i, i+1), m-1, result);
		}
		
	}

    public static void helper(String s, int m, String temp, ArrayList<String> result){
        
        // if(s.length() < m){
        //     return;
        // }
        
        //["ABCD"] m=4
        //If m == length of string s
        //Return s
        
        //m=1
        //['A','B','C','D']
        
        
        if(m==0){
            result.add(temp);
            return;
        }else{
            for(int i = 0; i < s.length(); i++){ 
                helper(s.substring(i+1), m-1, temp + s.substring(i,i+1), result);
                //helper(s.substring(i+1), m, temp, result);
            }
        }
        
    }
	
	public static void main(String[] args) {

		Combination("BC", 2);
	}

}
