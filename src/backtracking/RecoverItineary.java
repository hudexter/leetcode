package backtracking;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.*;

import org.junit.jupiter.api.Test;

public class RecoverItineary {

	public List<String> findItinerary(String[][] tickets) {
		for (String[] ticket : tickets) {
			// targets.computeIfAbsent(ticket[0], (a) -> {return new
			// PriorityQueue();}).add(ticket[1]);
			PriorityQueue q = targets.getOrDefault(ticket[0], new PriorityQueue<String>());
			q.add(ticket[1]);
			targets.put(ticket[0], q);
		}
		visit("JFK");
		return route;
	}

	Map<String, PriorityQueue<String>> targets = new HashMap<>();
	List<String> route = new LinkedList();

	void visit(String airport) {
		while (targets.containsKey(airport) && !targets.get(airport).isEmpty())
			visit(targets.get(airport).poll());
		
		route.add(0, airport);
	}

	@Test
	void test1() {
		assertEquals(Arrays.asList(new String[]{"JFK","ATL","JFK","SFO","ATL","SFO"}), new RecoverItineary().findItinerary(new String[][]{ { "JFK", "SFO" }, { "JFK", "ATL" }, { "SFO", "ATL" }, { "ATL", "JFK" },
			{ "ATL", "SFO" } } )
				);
		

		assertEquals(Arrays.asList(new String[]{"JFK", "MUC", "LHR", "SFO", "SJC"}),
				new RecoverItineary().findItinerary(new String[][] {{"MUC", "LHR"}, {"JFK", "MUC"}, {"SFO", "SJC"}, {"LHR", "SFO"}})
				);
	}

}
