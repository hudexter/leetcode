package math;

import java.util.HashMap;
import java.util.*;

public class RandomizedSet {

	HashMap<Integer, Integer> map;
	ArrayList<Integer> list;
	Random rand = new Random();

	/** Initialize your data structure here. */
	public RandomizedSet() {
		map = new HashMap<>();
		list = new ArrayList<Integer>();
	}

	/**
	 * Inserts a value to the set. Returns true if the set did not already contain
	 * the specified element.
	 */
	public boolean insert(int val) {
		if (!map.containsKey(val)) {
			list.add(val);
			map.put(val, list.size() - 1);
		}

		return false;
	}

	/**
	 * Removes a value from the set. Returns true if the set contained the specified
	 * element.
	 */
	public boolean remove(int val) {
		if (map.containsKey(val)) {
			int index = map.get(val);
			if (index < list.size() - 1) {
				list.add(index, list.get(list.size() - 1));
			}
			list.remove(list.size() - 1);
			map.remove(val);
			return true;
		}
		return false;
	}

	/** Get a random element from the set. */
	public int getRandom() {
		return list.get(rand.nextInt(list.size()));
	}

}
