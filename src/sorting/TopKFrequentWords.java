package sorting;

import java.util.*;
import java.util.Map.Entry;

/*
 * https://leetcode.com/problems/top-k-frequent-elements/description/
 */
public class TopKFrequentWords {

	public List<Integer> topKFrequent(int[] nums, int k) {

		HashMap<Integer, Integer> map = new HashMap<>();
		for (int a : nums) {
			map.put(a, map.getOrDefault(a, 0) + 1);
		}

		List<Integer>[] buckets = new List[nums.length + 1];

		for (int key : map.keySet()) {
			int val = map.get(key);
			if (buckets[val] == null) {
				buckets[val] = new ArrayList<Integer>();
				buckets[val].add(key);
			} else {
				buckets[val].add(key);
			}
		}

		List<Integer> result = new ArrayList<Integer>();

		for (int i = buckets.length - 1, j = 0; i >= 1; i--) {
			if (buckets[i] != null) {
				for (int a : buckets[i]) {
					result.add(a);
					j++;
					if (j >= k)
						return result;
				}
			}
		}

		return result;
	}

	public List<Integer> topKFrequent1(int[] nums, int k) {
		Map<Integer, Integer> map = new HashMap<>();
		Arrays.stream(nums).forEach(a -> map.put(a, map.getOrDefault(a, 0) + 1));
		PriorityQueue<Map.Entry<Integer, Integer>> minheap = new PriorityQueue<>((a, b) -> {
			return a.getValue() - b.getValue();
		});
		for (Map.Entry<Integer, Integer> e : map.entrySet()) {
			minheap.add(e);
			if (minheap.size() > k) {
				minheap.poll();
			}
		}
		List<Integer> result = new ArrayList<Integer>();
		while (!minheap.isEmpty()) {
			result.add(0, minheap.poll().getKey());
		}
		return result;
	}

}
