package trie;

import java.util.HashMap;

public class MagicDictionary {

	/** Initialize your data structure here. */
	public TrieNode root;

	public MagicDictionary() {
		root = new TrieNode();
	}

	/** Build a dictionary through a list of words */
	public void buildDict(String[] dict) {
		for (String s : dict) {
			this.insert(root, 0, s);
		}
	}

	/**
	 * Returns if there is any word in the trie that equals to the given word after
	 * modifying exactly one character
	 */
	public boolean search(String word) {

		char[] chars = word.toCharArray();
		for (int i = 0; i < word.length(); i++) {
			char a = word.charAt(i);
			for (int j = 0; j < 26; j++) {
				char c = (char) ('a' + j);
				if (c != a) {
					chars[i] = c;
					if (root.search(root, String.valueOf(chars)))
						return true;
					chars[i] = a;
				}
			}

		}

		return false;
	}

	public void insert(TrieNode root, int start, String word) {
		if (start < word.length()) {
			char c = word.charAt(start);
			if (root.map.containsKey(c)) {
				insert(root.map.get(c), start + 1, word);
			} else {
				root.map.put(c, new TrieNode());
				
				insert(root.map.get(c), start + 1, word);
			}
		}else {
			root.isend=true;
		}
	}

	private class TrieNode {
		HashMap<Character, TrieNode> map;
		boolean isend = false;

		public TrieNode() {
			map = new HashMap<>();
			isend = false;
		}

		boolean search(TrieNode root, String word) {
			if (word.length() == 0) {
				return root.isend;
			}
			else {
				char c = word.charAt(0);
				if (root.map.containsKey(c)) {
					return search(root.map.get(c), word.substring(1));
				} else {
					return false;
				}
			}

		}
	}

	public static void main(String[] args) {
		MagicDictionary obj = new MagicDictionary();
		String[] dict = { "hello", "leetcode" };
		obj.buildDict(dict);
		boolean param_2 = obj.search("hell");
		System.out.println(param_2);
		System.out.println(obj.root.search(obj.root, "hell"));
	}
}

/**
 * Your MagicDictionary object will be instantiated and called as such:
 * MagicDictionary obj = new MagicDictionary(); obj.buildDict(dict); boolean
 * param_2 = obj.search(word);
 */
