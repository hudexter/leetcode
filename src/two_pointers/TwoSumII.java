package two_pointers;
/*
 * https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/description/
 */
public class TwoSumII {

	public int[] twoSum(int[] nums, int target) {
		for(int i = 0, j = nums.length-1; i < j; ) {
			int sum = nums[i]+nums[j];
			if(sum == target) {
				return new int[] {i, j};
			}else if (sum < target){
				i++;
			}else {
				j--;
			}
		}
		return new int[] {};
	}
}
