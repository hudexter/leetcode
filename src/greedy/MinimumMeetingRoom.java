package greedy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MinimumMeetingRoom {
	
	class Timestamp {
		int time;
		boolean isStart;
//		public int compareTo(Timestamp other) {
//			return this.time - other.time;
//		}
		
		
		public Timestamp(int time, boolean start) {
			this.time = time;
			this.isStart = start;
		}

	}

	public int minMeetingrooms(int[][] timerange) {

		ArrayList<Timestamp> times = new ArrayList<Timestamp>();
		
		for(int[] a : timerange) {
			times.add(new Timestamp(a[0], true));
			times.add(new Timestamp(a[1], false));
		}
		
		

		Collections.sort(times, new Comparator<Timestamp>() {
		public int compare(Timestamp a, Timestamp b) {
			return a.time - b.time;
		}
		});
		System.out.println(times);
		
		int cur_room = 0;
		int max = Integer.MIN_VALUE;

		for (int i = 0; i < times.size(); i++) {
			Timestamp cur = times.get(i);
			if (cur.isStart) {
				cur_room++;
				max = Math.max(max, cur_room);
			} else {
				cur_room--;
			}
		}
		return max;
	}

	public static void main(String[] args) {
		MinimumMeetingRoom s = new MinimumMeetingRoom();
		int[][] meetings = { { 0, 30 }, { 5, 10 }, { 15, 20 } };
		System.out.println(meetings.length);
		System.out.println(s.minMeetingrooms(meetings));
	}

}
