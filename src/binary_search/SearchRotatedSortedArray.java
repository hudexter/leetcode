package binary_search;

/**
 * Created by gouthamvidyapradhan on 10/04/2017. Suppose an array sorted in
 * ascending order is rotated at some pivot unknown to you beforehand.
 * 
 * (i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).
 * 
 * You are given a target value to search. If found in the array return its
 * index, otherwise return -1.
 * 
 * You may assume no duplicate exists in the array.
 * 
 */
public class SearchRotatedSortedArray {
	/**
	 * Main method
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		int[] A = {  1, 2, 3 };
		// System.out.println(new SearchRotatedSortedArray().search(A, 4));
		//System.out.println(binarySearch(A, 0, 1, 2));
		System.out.println(shiftedArrSearch(A, 2));
	}

	public int search(int[] nums, int target) {
		if (nums.length == 0)
			return -1;
		if (nums.length == 1) {
			return (nums[0] == target) ? 0 : -1;
		}
		int low = 0, high = nums.length - 1;
		while (low < high) {
			int mid = (low + high) >>> 1;
			if (nums[mid] == target)
				return mid;
			if ((nums[mid] <= nums[low]) && (target > nums[mid] && target <= nums[high])
					|| (nums[low] <= nums[mid] && (target < nums[low] || target > nums[mid])))
				low = mid + 1;
			else
				high = mid - 1;
		}
		return (nums[low] == target) ? low : -1;
	}

	static int shiftedArrSearch(int[] shiftArr, int num) {
		// your code goes here

		return binarySearchShiftedArray(shiftArr, 0, shiftArr.length - 1, num);
	}

	static int binarySearchShiftedArray(int[] shiftArr, int left, int right, int num) {
		
		if(left>right)
			return -1;

		int mid = (left + right) / 2;

		if (shiftArr[mid] == num) {
			return mid;
		}


		int result = -1;

		boolean rightHalfNotSorted = shiftArr[mid] >= shiftArr[right];

		if (rightHalfNotSorted) {
			result = binarySearch(shiftArr, left, mid - 1, num);

			if (result != -1) {
				return result;
			}

			return binarySearchShiftedArray(shiftArr, mid + 1, right, num);
		} else {
			result = binarySearch(shiftArr, mid + 1, right, num); // normal

			if (result != -1) {
				return result;
			}

			return binarySearchShiftedArray(shiftArr, left, mid - 1, num);
		}
	}

	static int binarySearch(int[] arry, int start, int end, int num) {

		int low = start;
		int high = end;

		while (low <= high) {
			//System.out.println(high);
			int mid = (low + high) / 2;
			//System.out.println(mid);
			int value = arry[mid];

			if (value == num) {
				return mid;
			} else if (value < num) {
				low = mid + 1;
			} else {
				high = mid - 1;
			}
		}

		return -1;

	}

}
