package hashing;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

public class firstUniqChar {
	
    public int firstUniqChar(String s) {
    LinkedHashMap<Character, Integer> map = new LinkedHashMap<>();
		for (int i = 0; i < s.length(); i++) {
			char a = s.charAt(i);
			if (map.containsKey(a)) {
				map.put(a, map.get(a) + 1);
			} else {
				map.put(a, 1);
			}
		}

		for (Entry<Character, Integer> entry : map.entrySet()) {
			if (entry.getValue() == 1) {
				char a = entry.getKey();
				return s.indexOf(a);
			}
		}

		return -1;
    }

}
