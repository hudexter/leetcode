package hashing;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Stack;

/**
 * Created by gouthamvidyapradhan on 09/03/2017. Given an array of integers,
 * return indices of the two numbers such that they add up to a specific target.
 * 
 * You may assume that each input would have exactly one solution, and you may
 * not use the same element twice.
 * 
 * Example: Given nums = [2, 7, 11, 15], target = 9,
 * 
 * Because nums[0] + nums[1] = 2 + 7 = 9, return [0, 1].
 */
public class TwoSum {
	HashMap<Integer, Integer> map = new HashMap<>();

	public int[] twoSum(int[] nums, int target) {
		int[] result = new int[2];

		for (int i : nums) {
			if (map.keySet().contains(i)) {
				int count = map.get(i);
				map.put(i, ++count);
			} else {
				map.put(i, 1);
			}
		}

		for (int i = 0, l = nums.length; i < l; i++) {
			int ele = nums[i];
			int req = target - ele;
			if (map.keySet().contains(req)) {
				result[0] = i;
				if (ele == req) {
					int count = map.get(req);
					if (count > 1) {
						for (int j = i + 1; j < l; j++) {
							if (nums[j] == req) {
								result[1] = j;
								return result;
							}
						}
					}
				} else {
					for (int j = i + 1; j < l; j++) {
						if (nums[j] == req) {
							result[1] = j;
							return result;
						}
					}
				}
			}
		}
		return result;
	}

	public static String isMatched(String input) {
		HashMap<Character, Character> map = new HashMap<>();
		map.put('{', '}');
		map.put('[', ']');
		map.put('{', '}');

		Stack<Character> stack = new Stack<>();
		for (int i = 0; i < input.length(); i++) {
			char c = input.charAt(i);
			if (c == '{' || c == '[' || c == '(') {
				stack.push(c);
			} else if (c == ')' || c == '}' || c == ']') {
				if (stack.isEmpty()) {
					return "NO";
				} else {
					char top = stack.peek();
					System.out.println(top);
					if (map.get(top) == c) {
						stack.pop();
					} else {
						return "NO";
					}
				}

			}
		}

		if (stack.isEmpty()) {
			return "YES";
		}

		return "NO";
	}

	public static void main(String[] args) {
		System.out.println(isMatched("()[]{}"));
	}

	static int palindrome(String s) {
		HashSet<String> set = new HashSet<>();
		for(int i = 0; i < s.length(); i++) {
			for(int j = i+1; j<s.length(); j++) {
				if(isPalindrome(s, i, j)) {
					set.add(s.substring(i, j+1));
				}
			}
		}
		return set.size();
	}

	static boolean isPalindrome(String s, int i, int j) {
		while (i < j) {
			if (s.charAt(i) == s.charAt(j)) {
				i++;
				j--;
			} else {
				return false;
			}
		}
		return true;
	}

}
