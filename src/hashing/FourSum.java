package hashing;

import java.util.HashMap;
import java.util.Map.Entry;

public class FourSum {
	
	public int fourSumCount(int[] A, int[] B, int[] C, int[] D) {
        int count = 0;
        HashMap<Integer, Integer> absum = new HashMap<>();
        for(int i = 0; i< A.length; i++){
            for(int j = 0; j< B.length; j++){
                absum.put(A[i]+B[j], absum.getOrDefault(A[i]+B[j], 1));
            }
        }
        
        HashMap<Integer, Integer> cdsum = new HashMap<>();
        
        for(int i = 0; i< C.length; i++){
            for(int j = 0; j< D.length; j++){
                cdsum.put(C[i]+D[j], cdsum.getOrDefault(C[i]+D[j], 1));
            }
        }
        
        
        for(Entry<Integer, Integer> e: absum.entrySet()) {
        		if(cdsum.containsKey(-(e.getKey()))) {
        			count += cdsum.get(-(e.getKey()))*e.getValue();
        		}
        }
        
       return count; 
    }
	
	public static void main(String args[]) {
	}
}
