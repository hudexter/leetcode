package hashset;

import java.util.HashSet;

public class SentenceSimilarityI {
	
	class Pair {
		String a;
		String b;
		public Pair(String string, String string2) {
			// TODO Auto-generated constructor stub
			a = string;
			b = string2;
		}

		@Override
		public boolean equals(Object o) {
			// TODO Auto-generated method stub
			Pair target = (Pair) o;
			return a.equals(target.a) && b.equals(target.b);
			
		}
		
		@Override
		public int hashCode() {
			
			return a.hashCode() * b.hashCode();
			
		}
	}
	public boolean areSentencesSimilar(String[] words1, String[] words2, String[][] pairs) {
		HashSet<Pair> set = new HashSet<Pair>();
		for(int i = 0; i < pairs.length; i++) {
			set.add(new Pair(pairs[i][0], pairs[i][1]));
			set.add(new Pair(pairs[i][1], pairs[i][0]));
		}
		
		if(words1.length != words2.length) {
			return false;
		}
		
		for(int i = 0; i < words1.length; i++) {
            if(words1[i].equals(words2[i]))
                continue;
			if(set.contains(new Pair(words1[i], words2[i])))
				continue;
			if(set.contains(new Pair(words2[i], words1[i])))
				continue;
			
			return false;
		}
		
		return true;
	}
}
